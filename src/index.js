import * as THREE from 'three';
import {RGBELoader} from 'three/examples/jsm/loaders/RGBELoader';
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
import Stats from 'three/examples/jsm/libs/stats.module';
import TWEEN from '@tweenjs/tween.js';
import './styles/styles.css';
import 'core-js/stable';
import 'regenerator-runtime/runtime';

class TestScene {
	/**
	 * @param options {{
	 *    containerSelector?: string,
	 *    debug?: boolean,
	 *    antialias?: boolean
	 *  }}
	 * @return {TestScene}
	 */
	constructor(options = {}) {
		if (TestScene.exists) {
			return TestScene.instance;
		}

		TestScene.instance = this;
		TestScene.exists = true;

		this.containerSelector = options.containerSelector || '#container';
		this.container = document.querySelector(this.containerSelector);

		this.debug = options.debug || false;

		// this.inst_init = false;

		this.progress = {
			percent: 0,
			loadedFiles: 0,
			loadedTotal: 0,
			fileLink: null,
			error: null,
		};

		// init app
		this.app = {
			mixer: null,
			clock: new THREE.Clock(),
			renderer: new THREE.WebGLRenderer({
				antialias: {}.hasOwnProperty.call(options, 'antialias') ? options.antialias : true,
				alpha: true,
				logarithmicDepthBuffer: true
			}),
			camera: new THREE.PerspectiveCamera(45, this.container.clientWidth / this.container.clientHeight, 1, 2000),
			controls: null,
			scene: new THREE.Scene(),
		};

		if (this.debug) {
			this.app.stats = new Stats();
			this.container.appendChild(this.app.stats.dom);
		}

		this.init(() => {
			if (this.debug) {
				console.log(this.debug);
			} else {
				console.log(this.debug);
			}
			this.updater();
		});
	}

	init(cb = () => {}) {
		const progress = this.progress;
		const pmremGenerator = new THREE.PMREMGenerator(this.app.renderer);

		// init lights
		const hemiLight = new THREE.HemisphereLight(0x0026ff, 0xff6a00, 5);
		const directLight = new THREE.DirectionalLight(0xfffacd, 12.5);

		// init controls
		this.app.controls = new OrbitControls(this.app.camera, this.app.renderer.domElement);
		this.app.controls.target.set(0, 0, 0);
		this.app.controls.enabled = this.debug;
		this.app.controls.update();

		this.container.appendChild(this.app.renderer.domElement);

		// main scene config
		this.app.camera.position.set(0, 15, 25);
		this.app.camera.near = 0.1;
		this.app.camera.far = 500;
		this.app.camera.updateProjectionMatrix();

		hemiLight.position.set(0, 100, 0);

		directLight.position.set(-5, 20, 15);
		directLight.target.position.set(0,0.75,0);
		directLight.castShadow = true;
		directLight.shadow.camera.near = 1;
		directLight.shadow.camera.far = 500;
		directLight.shadow.bias = 0.000001;
		directLight.shadow.camera.top = 180;
		directLight.shadow.camera.bottom = -100;
		directLight.shadow.camera.left = -120;
		directLight.shadow.camera.right = 120;
		directLight.shadow.mapSize.width = 512;
		directLight.shadow.mapSize.height = 512;
  
		this.app.scene.add(directLight, directLight.target, hemiLight);
  
		this.app.renderer.setClearColor(0x000000, 0);
		this.app.renderer.outputEncoding = THREE.sRGBEncoding;
		this.app.renderer.physicallyCorrectLights = true;
		this.app.renderer.toneMapping = THREE.ACESFilmicToneMapping;
		this.app.renderer.toneMappingExposure = 0.2;
		this.app.renderer.setPixelRatio(window.devicePixelRatio);
		this.app.renderer.setSize(this.container.clientWidth, this.container.clientHeight);
		this.app.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
		this.app.renderer.shadowMap.enabled = !this.featureAvailable('MOBILE');
  
		pmremGenerator.compileEquirectangularShader();
  
		// ground
		const mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(2000, 2000), new THREE.ShadowMaterial({
			opacity: 0.2
		}));
		mesh.rotation.x = -Math.PI / 2;
		mesh.receiveShadow = true;
		this.app.scene.add(mesh);
  
		if (this.debug) {
			const axesHelper = new THREE.AxesHelper(5);
   
			directLight.target.updateMatrixWorld();
			const dirLightHelper = new THREE.DirectionalLightHelper(directLight, 5);
   
			this.app.scene.add(dirLightHelper, axesHelper);

			const grid = new THREE.GridHelper(1000, 1000, 0x000000, 0x000000);
			grid.material.opacity = 0.1;
			grid.material.transparent = true;
			this.app.scene.add(grid);

		}

		// init loading manager for start load
		const managerMain = new THREE.LoadingManager();
		managerMain.onStart = (url, itemsLoaded, itemsTotal) => {
			progress.fileLink = url;
			progress.loadedFiles = itemsLoaded;
			progress.loadedTotal = itemsTotal;
		};
		managerMain.onLoad = () => {
			this.app.controls.update();
			progress.percent = 100;
			console.log('Scene is ready');
			cb();
		};
		managerMain.onProgress = (url, itemsLoaded, itemsTotal) => {
			progress.fileLink = url;
			progress.loadedFiles = itemsLoaded;
			progress.loadedTotal = itemsTotal;
		};

		managerMain.onError = function (url) {
			progress.error = url;
		};

		// init HDR lighting
		new RGBELoader(managerMain)
			.setDataType(THREE.UnsignedByteType)
			.setPath('./assets/env/')
			.load('hq1.hdr', (hdrEquirectangular) => {
    
				// this.app.scene.background = hdrCubeRenderTarget;
				this.app.scene.environment = pmremGenerator.fromEquirectangular(hdrEquirectangular).texture;

				// this.app.scene.getObjectByName('hair_transparent').material.envMap = hdrCubeRenderTarget;
    
				this.app.renderer.render(this.app.scene, this.app.camera);
    
				hdrEquirectangular.dispose();
				pmremGenerator.dispose();
			}, null, this.error);
		
		const gltfURL = '';
		
		if (gltfURL) {
			new GLTFLoader(managerMain).load(gltfURL, gltf => {
				this.app.scene.add(gltf.scene);
			});
		}


		window.addEventListener('resize', this.onWindowResize.bind(this), false);

	}

	error(error) {
		console.error(error);
	}
 
	onWindowResize() {

		this.app.camera.aspect = window.innerWidth / window.innerHeight;
		this.app.camera.updateProjectionMatrix();

		this.app.renderer.setSize(window.innerWidth, window.innerHeight);

	}

	// pushToBefore(arr, name, length = 2) {
	//     if (arr.length >= length) {
	//         arr.length = 1;
	//     }
	//     arr.unshift(name);
	// }

	getProgress() {
		return this.progress;
	}

	// randomInteger(min, max) {
	// // получить случайное число от (min) до (max)
	//     return min + Math.random() * (max - min);
	// }

	updater() {

		requestAnimationFrame(() => this.updater());

		TWEEN.update();

		this.app.renderer.render(this.app.scene, this.app.camera);

		if (this.debug) {
			this.app.stats.update();
		}

	}

	featureAvailable(feature) {

		const {
			userAgent
		} = window.navigator;
		const {
			platform
		} = window.navigator;

		const canvas = document.createElement('canvas');
		const gl = canvas.getContext('webgl2');

		switch (feature) {
		case 'LINUX':
			return /Linux/.test(platform);
		case 'WINDOWS':
			return ['Win32', 'Win64', 'Windows', 'WinCE'].indexOf(platform) !== -1;
		case 'MACOS':
			return ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'].indexOf(platform) !== -1;
		case 'IOS':
			return (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream);
		case 'ANDROID':
			return /Android/i.test(userAgent);
		case 'MOBILE':
			return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(userAgent);

		case 'CHROME':
			// Chromium based
			return (!!window['chrome'] && !/Edge/.test(navigator.userAgent));
		case 'FIREFOX':
			return /Firefox/.test(navigator.userAgent);
		case 'IE':
			return /Trident/.test(navigator.userAgent);
		case 'EDGE':
			return /Edge/.test(navigator.userAgent);
		case 'SAFARI':
			return (/Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgent));

		case 'TOUCH':
			return !!(('ontouchstart' in window) || window.navigator.maxTouchPoints > 0);
		case 'RETINA':
			return window.devicePixelRatio >= 2;
		case 'WEBGL2':
			return !!gl;
		case 'WOOCOMMERCE':
			return !!window.parent['v3d_woocommerce_change_param'];
		default:
			return false;
		}
	}

}

window.TestScene = TestScene;

window.addEventListener('load', () => {
	console.log({THREE: THREE.REVISION});
	const app = new TestScene({debug: true, antialias: true});
	console.log({app});
});
