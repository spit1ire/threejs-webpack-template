const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const webpack = require('webpack');

const isDev = process.env.NODE_ENV === 'development';
const isProd = !isDev;

const optimization = () => {
    const config = {
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
            name: 'vendors'
        }
    };

    if (isProd) {
        config.minimizer = [
            new CssMinimizerPlugin(),
            new TerserWebpackPlugin(),
        ];
    }

    return config;
};

const filename = ext => isDev ? `[name].${ext}` : `[name].[fullhash].${ext}`;

const babelOptions = preset => {
    const opts = {
        presets: [
            '@babel/preset-env'
        ],
        plugins: [
            '@babel/plugin-proposal-class-properties'
        ],
        compact: false
    };

    if (preset) {
        opts.presets.push(preset);
    }

    return opts;
};

const jsLoaders = () => {
    // const loaders =
    return [{
        loader: 'babel-loader',
        options: babelOptions()
    }];

    // if (isDev) {
    //     loaders.push('eslint-loader');
    // }
  
    // return loaders;
};

module.exports = {
    target: "web",
    context: path.resolve(__dirname, 'src'),
    mode: 'development',
    // node: {
    //   fs: "empty"
    // },
    resolve: {
        fallback: {
            fs: false,
            path: require.resolve("path-browserify")
        }
    },
    entry: {
        main: './index.js',
        styles: './styles.js'
    },
    performance: {
        maxEntrypointSize: 1536000,
        maxAssetSize: 50000000,
    },
    output: {
        filename: filename('js'),
        path: path.resolve(__dirname, 'dist'),
    },
    optimization: optimization(),
    devServer: {
        port: '4422',
        hot: isDev,
        host: '0.0.0.0',
        compress: true,
    // disableHostCheck: true
    },
    devtool: isDev ? 'source-map' : false,
    plugins: [
        new ESLintPlugin(),
        new HtmlWebpackPlugin({
            template: './index.html',
            minify: {
                collapseWhitespace: isProd
            }
        }),
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: path.resolve(__dirname, 'src/assets'),
                    to: path.resolve(__dirname, 'dist/assets')
                },
                {
                    from: path.resolve(__dirname, 'src/imgs'),
                    to: path.resolve(__dirname, 'dist/imgs')
                },
            ]
        }),
        new MiniCssExtractPlugin({
            filename: filename('css')
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.(png|jpg|svg|gif)$/,
                use: ['file-loader']
            },
            { 
                test: /\.js$/,
                exclude: /node_modules/,
                use: jsLoaders()
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            }
        ]
    }
};
